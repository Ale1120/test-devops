#!/bin/bash


minikube start
eval $(minikube docker-env) 

#create imagen docker

docker build -t api /api .
docker build -t listaner /listener .

minikube cache add api
minikube cache add listaner
minikube cache reload


# deploment
minikube kubectl apply -f /postgres/postgres-configmap.yaml
minikube kubectl apply -f /postgres/postgres-storage.yaml
minikube kubectl apply -f /postgres/postgres-deploment.yaml
minikube kubectl apply -f /postgres/postgres-service.yaml
minikube kubectl apply -f /api/api-deployment.yaml
minikube kubectl apply -f /api/api-service.yaml
minikube kubectl apply -f /listener/listener.yaml